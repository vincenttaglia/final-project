package view;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import controller.CommandHistory;
import controller.MoveCommand;
import controller.SelectCommand;
import controller.ShapeBuilder;
import controller.interfaces.IGroupable;
import controller.interfaces.IShape;
import model.persistence.ApplicationState;
import model.persistence.GroupableList;
import model.persistence.SelectList;

public class MouseHandler implements MouseListener {
	
	private Point startPoint;
	private Point endPoint;
	private ApplicationState applicationState;
	
	public MouseHandler(ApplicationState applicationState) {
		this.applicationState = applicationState;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		startPoint = new Point((int)e.getX(), (int)e.getY());
		//System.out.println(startPoint.toString());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		endPoint = new Point((int)e.getX(), (int)e.getY());
		if(!startPoint.equals(endPoint)) {
			//System.out.println(endPoint.toString());
			
			switch(applicationState.getActiveMouseMode()) {
				case DRAW:
					IShape newShape = new ShapeBuilder().setCurrentState(applicationState).setByPoints(startPoint, endPoint).build();
					GroupableList.getInstance().push(newShape);
					newShape.run();
					CommandHistory.getInstance().add(newShape);
					break;
				case SELECT:
					SelectCommand.select(applicationState, startPoint, endPoint);
					break;
				case MOVE:
					MoveCommand mv = new MoveCommand(new ArrayList<IGroupable>(SelectList.getInstance().getSelectList()), startPoint, endPoint);
					mv.run();
					CommandHistory.getInstance().add(mv);
					break;
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		startPoint = new Point((int)e.getX(), (int)e.getY());
		endPoint = new Point((int)e.getX(), (int)e.getY());
		//System.out.println(endPoint.toString());
		
		switch(applicationState.getActiveMouseMode()) {
		case DRAW:
			break;
		case SELECT:
			SelectCommand.select(applicationState, startPoint);
			break;
		case MOVE:
			break;
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
