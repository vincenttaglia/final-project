package view;

public class Point {
	private int x;
	private int y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Point getPoint() {
		return this;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public String toString() {
		return x + ", " + y;
	}
	
	public Boolean equals(Point point) {
		return point.getX() == x && point.getY() == y;
	}
}
