package view.gui;

import view.interfaces.PaintCanvasBase;

import javax.swing.JComponent;

import java.awt.*;

public class PaintCanvas extends PaintCanvasBase {
	
	private static PaintCanvas instance;
	
	private PaintCanvas() {}
	
	public static PaintCanvas getInstance() {
		if(instance == null)
            instance = new PaintCanvas();

        return instance;
	}
	
    public Graphics2D getGraphics2D() {
        return (Graphics2D)getGraphics();
    }
}
