package model;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

public class ShapeColorMap {
	private static Map<ShapeColor, Color> mapOfColors = new HashMap<>();
	
	static {
		mapOfColors.put(ShapeColor.BLACK, Color.BLACK);
		mapOfColors.put(ShapeColor.BLUE, Color.BLUE);
		mapOfColors.put(ShapeColor.CYAN, Color.CYAN);
		mapOfColors.put(ShapeColor.DARK_GRAY, Color.DARK_GRAY);
		mapOfColors.put(ShapeColor.GRAY, Color.GRAY);
		mapOfColors.put(ShapeColor.GREEN, Color.GREEN);
		mapOfColors.put(ShapeColor.LIGHT_GRAY, Color.LIGHT_GRAY);
		mapOfColors.put(ShapeColor.MAGENTA, Color.MAGENTA);
		mapOfColors.put(ShapeColor.ORANGE, Color.ORANGE);
		mapOfColors.put(ShapeColor.PINK, Color.PINK);
		mapOfColors.put(ShapeColor.RED, Color.RED);
		mapOfColors.put(ShapeColor.WHITE, Color.WHITE);
		mapOfColors.put(ShapeColor.YELLOW, Color.YELLOW);
	}

	public static Color getColor(ShapeColor shapeColor) {
		return mapOfColors.get(shapeColor);
	}
}
