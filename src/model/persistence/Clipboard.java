package model.persistence;

import java.util.ArrayList;

import controller.interfaces.IGroupable;

public class Clipboard {
	private static Clipboard instance = new Clipboard();
	private ArrayList<IGroupable> clipboard = null;
	
	private Clipboard() {
		clipboard = new ArrayList<IGroupable>();
	}
	
	public static Clipboard getInstance() {
        if(instance == null)
            instance = new Clipboard();

        return instance;
    }
	
	public ArrayList<IGroupable> getClipboard(){
		return clipboard;
	}
	
	public void copy() {
		clipboard = new ArrayList<IGroupable>(SelectList.getInstance().getSelectList());
	}
	
	
	
}
