package model.persistence;

import java.util.ArrayList;

import controller.interfaces.IGroupable;

public final class GroupableList {
	
	private static GroupableList instance = new GroupableList();
	private ArrayList<IGroupable> groupableList = null;
	
	private GroupableList() {
		this.groupableList = new ArrayList<IGroupable>();
	}
	
	public static GroupableList getInstance() {
        if(instance == null)
            instance = new GroupableList();

        return instance;
    }
	
	public ArrayList<IGroupable> getGroupableList(){
		return groupableList;
	}
	
	public void push(IGroupable shape) {
		this.groupableList.add(shape);
	}
	
	public void push(ArrayList<IGroupable> groupableList) {
		this.groupableList.addAll(groupableList);
	}
	
	public void remove(IGroupable shape) {
		this.groupableList.remove(shape);
	}
	
	public void remove(ArrayList<IGroupable> groupableList) {
		this.groupableList.removeAll(groupableList);
	}
		
}
