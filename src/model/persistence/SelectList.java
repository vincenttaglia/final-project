package model.persistence;

import java.util.ArrayList;

import controller.interfaces.IGroupable;

public class SelectList {
	private static SelectList instance = new SelectList();
	private ArrayList<IGroupable> selectList = null;
	
	private SelectList() {
		selectList = new ArrayList<IGroupable>();
	}
	
	public static SelectList getInstance() {
        if(instance == null)
            instance = new SelectList();

        return instance;
    }
	
	public ArrayList<IGroupable> getSelectList(){
		return selectList;
	}
	
	public void select(IGroupable shape) {
		shape.setSelected(true);
		selectList.add(shape);
		//System.out.println(selectList);
	}
	
	public void singleSelect(IGroupable shape) {
		this.clear();
		shape.setSelected(true);
		selectList.add(shape);
	}
	
	public void deselect(IGroupable shape) {
		selectList.remove(shape);
		//System.out.println(selectList);
	}
	
	public void clear() {
		for(IGroupable s : selectList)
			s.setSelected(false);
		selectList.clear();
		//System.out.println(selectList);
	}
}
