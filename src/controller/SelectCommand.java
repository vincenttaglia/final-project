package controller;

import controller.interfaces.IGroupable;
import model.persistence.ApplicationState;
import model.persistence.GroupableList;
import model.persistence.SelectList;
import view.Point;

public class SelectCommand {
	public static void select(ApplicationState applicationState, Point startPoint, Point endPoint) {
		SelectList.getInstance().clear();
		GroupableList.getInstance().getGroupableList().forEach((s) -> {
			if(s.isIncludedIn(startPoint, endPoint))
				SelectList.getInstance().select(s);
		});

		RedrawCommand.run();
	}
	
	public static void select(ApplicationState applicationState, Point point) {
		SelectList.getInstance().clear();
		for(IGroupable s : GroupableList.getInstance().getGroupableList()) {
			if(s.includesPoint(point)) {
				SelectList.getInstance().singleSelect(s);
			}
		}
		
		RedrawCommand.run();
		
	}
}
