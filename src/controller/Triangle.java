package controller;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import controller.interfaces.IShape;
import model.ShapeShadingType;
import model.ShapeType;
import view.gui.PaintCanvas;
import view.interfaces.PaintCanvasBase;

public class Triangle implements IShape{

	private Color primaryColor;
	private Color secondaryColor;
	private int startX;
	private int startY;
	private int width;
	private int height;
	private ShapeShadingType shadingType;
	private ShapeType shapeType;
	private Boolean selected = false;

	public Triangle(Color primaryColor, Color secondaryColor, int startX, int startY, int width, int height, ShapeShadingType shadingType, ShapeType shapeType) {
		this.primaryColor = primaryColor;
		this.secondaryColor = secondaryColor;
		this.startX = startX;
		this.startY = startY;
		this.width = width;
		this.height = height;
		this.shadingType = shadingType;
		this.shapeType = shapeType;
		
	}
	
	@Override
	public Color getColor() {
		return primaryColor;
	}
	
	@Override
	public Color getSecondaryColor() {
		return secondaryColor;
	}

	@Override
	public int getStartX() {
		return startX;
	}

	@Override
	public int getStartY() {
		return startY;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}
	
	@Override
	public ShapeShadingType getShadingType() {
		return shadingType;
	}
	
	@Override
	public ShapeType getShapeType() {
		return shapeType;
	}
	
	@Override
	public Boolean isSelected() {
		return selected;
	}
	
	@Override
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@Override
	public void run() {
		PaintCanvasBase paintCanvasBase = PaintCanvas.getInstance();
		Graphics2D graphics = paintCanvasBase.getGraphics2D();
		
		int[] Xs = new int[3];
		int[] Ys = new int[3];
		
		Xs[0] = getStartX(); // Bottom left
		Xs[1] = getStartX() + getWidth(); // Bottom right
		Xs[2] = getStartX() + getWidth()/2; // Top
		
		Ys[0] = getStartY() + getHeight(); // Bottom left
		Ys[1] = getStartY() + getHeight(); // Bottom right
		Ys[2] = getStartY(); // Top
		
		switch(getShadingType()) {
		case FILLED_IN:
			graphics.setColor(getColor());
			graphics.fillPolygon(Xs, Ys, 3);
			break;
		case OUTLINE:
			graphics.setColor(getColor());
			graphics.setStroke(new BasicStroke(3));
			graphics.drawPolygon(Xs, Ys, 3);
			break;
		case OUTLINE_AND_FILLED_IN:
			graphics.setColor(getColor());
			graphics.fillPolygon(Xs, Ys, 3);
			graphics.setColor(getSecondaryColor());
			graphics.setStroke(new BasicStroke(3));
			graphics.drawPolygon(Xs, Ys, 3);
			break;
		}
		
		if(isSelected()) {
			Xs[0] = getStartX() - 6; // Bottom left
			Xs[1] = getStartX() + getWidth() + 6; // Bottom right
			Xs[2] = getStartX() + getWidth()/2; // Top
			
			Ys[0] = getStartY() + getHeight() + 3; // Bottom left
			Ys[1] = getStartY() + getHeight() + 3; // Bottom right
			Ys[2] = getStartY() - 6; // Top
			
			graphics.setColor(Color.GRAY);
			Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
			graphics.setStroke(dashed);
			graphics.drawPolygon(Xs, Ys, 3);
		}
	}

}
