package controller;

import java.awt.Color;
import java.util.ArrayList;

import controller.interfaces.IGroupable;
import controller.interfaces.IShape;
import model.ShapeShadingType;
import model.ShapeType;

public class GroupableFactory {
	public static Group createGroup(ArrayList<IGroupable> items) {
		if(items.size() > 1) {
			int groupStartX = items.get(0).getStartX();
			int groupStartY = items.get(0).getStartY();
			int groupWidth = items.get(0).getWidth();
			int groupHeight = items.get(0).getHeight();
			
			for(IGroupable item : items) {
				int groupEndX = groupStartX + groupWidth;
				int groupEndY = groupStartY + groupHeight;
				//System.out.println(item);
				int startX = item.getStartX();
				int startY = item.getStartY();
				int width = item.getWidth();
				int height = item.getHeight();
				int endX = startX + width;
				int endY = startY + height;
				
				if(startX < groupStartX) {
					int startDiff = groupStartX - startX;
					groupStartX = startX;
					groupWidth = groupWidth + startDiff;
				}
				if(startY < groupStartY) {
					int startDiff = groupStartY - startY;
					groupStartY = startY;
					groupHeight = groupHeight + startDiff;
				}
				
				if(endX > groupEndX)
					groupWidth = endX - groupStartX;
				if(endY > groupEndY)
					groupHeight = endY - groupStartY;
				
			}
			
			return new Group(items, groupStartX, groupStartY, groupWidth, groupHeight);
		}
		
		return null;
	}
	
	public static IShape createShape(Color primaryColor, Color secondaryColor, int startX, int startY, int width, int height, ShapeShadingType shadingType, ShapeType shapeType) {
		switch(shapeType) {
			case RECTANGLE:
				return new Rectangle(primaryColor, secondaryColor, startX, startY, width, height, shadingType, shapeType);
			   
			case ELLIPSE:
				return new Ellipse(primaryColor, secondaryColor, startX, startY, width, height, shadingType, shapeType);
			   
			case TRIANGLE:
				return new Triangle(primaryColor, secondaryColor, startX, startY, width, height, shadingType, shapeType);
		}
		return null;
	}
}
