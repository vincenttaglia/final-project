package controller;

import java.util.ArrayList;

import controller.interfaces.ICommand;
import controller.interfaces.IGroupable;
import controller.interfaces.IUndoable;
import model.persistence.Clipboard;
import model.persistence.GroupableList;

public class PasteCommand implements IUndoable, ICommand {
	ArrayList<IGroupable> pasteList;
	
	public PasteCommand() {
		this.pasteList = new ArrayList<IGroupable>();
		Clipboard clipboard = Clipboard.getInstance();
		
		for(IGroupable shape : clipboard.getClipboard())
			pasteList.add(shape.transform(300, 300));
		
		CommandHistory.getInstance().add(this);
	}

	@Override
	public void run() {
		GroupableList.getInstance().push(this.pasteList);
		RedrawCommand.run();
	}

	@Override
	public void undo() {
		GroupableList.getInstance().remove(this.pasteList);
		RedrawCommand.run();
	}

	@Override
	public void redo() {
		this.run();
	}
	
}
