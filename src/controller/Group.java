package controller;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.ArrayList;

import controller.interfaces.IGroupable;
import model.persistence.GroupableList;
import view.Point;
import view.gui.PaintCanvas;
import view.interfaces.PaintCanvasBase;

public class Group implements IGroupable {
	private int startX;
	private int startY;
	private int width;
	private int height;
	private Boolean selected = false;
	private ArrayList<IGroupable> groupArray;
	
	public Group(ArrayList<IGroupable> group, int startX, int startY, int width, int height) {
		this.groupArray = group;
		this.startX = startX;
		this.startY = startY;
		this.width = width;
		this.height = height;
	}

	@Override
	public int getStartX() {
		return startX;
	}

	@Override
	public int getStartY() {
		return startY;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public Boolean isSelected() {
		return selected;
	}

	@Override
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	public ArrayList<IGroupable> getGroupArray() {
		return groupArray;
	}
	
	@Override
	public IGroupable transform(Point startPoint, Point endPoint) {
		int x = endPoint.getX() - startPoint.getX();
		int y = endPoint.getY() - startPoint.getY();
		return transform(x,y);
	}

	@Override
	public IGroupable transform(int transformX, int transformY) {
		ArrayList<IGroupable> transformedGroupables = new ArrayList<IGroupable>();
		for(IGroupable item : groupArray) {
			transformedGroupables.add(item.transform(transformX, transformY));
		}
		
		return GroupableFactory.createGroup(transformedGroupables);
	}

	@Override
	public void undo() {
		GroupableList.getInstance().remove(this);
		GroupableList.getInstance().push(groupArray);
		if(isSelected()) {
			setSelected(false);
			for(IGroupable item : groupArray)
				item.setSelected(true);
		}
		RedrawCommand.run();
		
	}

	@Override
	public void redo() {
		GroupableList.getInstance().push(this);
		GroupableList.getInstance().remove(groupArray);
		setSelected(true);
		for(IGroupable item : groupArray)
			item.setSelected(false);
		RedrawCommand.run();
		
	}

	@Override
	public void run() {
		for(IGroupable item : groupArray) {
			item.run();	
		}
		
		if(isSelected()) {
			PaintCanvasBase paintCanvasBase = PaintCanvas.getInstance();
			Graphics2D graphics = paintCanvasBase.getGraphics2D();
			
			graphics.setColor(Color.GRAY);
			Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
			graphics.setStroke(dashed);
			graphics.drawRect(getStartX() - 5, getStartY() - 5, getWidth() + 10, getHeight() + 10);
		}
		
	}

	
}
