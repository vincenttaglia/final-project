package controller;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import controller.interfaces.IShape;
import model.ShapeShadingType;
import model.ShapeType;
import view.gui.PaintCanvas;
import view.interfaces.PaintCanvasBase;

public class Rectangle implements IShape {

	private Color primaryColor;
	private Color secondaryColor;
	private int startX;
	private int startY;
	private int width;
	private int height;
	private ShapeShadingType shadingType;
	private ShapeType shapeType;
	private Boolean selected = false;
	
	
	public Rectangle(Color primaryColor, Color secondaryColor, int startX, int startY, int width, int height, ShapeShadingType shadingType, ShapeType shapeType) {
		this.primaryColor = primaryColor;
		this.secondaryColor = secondaryColor;
		this.startX = startX;
		this.startY = startY;
		this.width = width;
		this.height = height;
		this.shadingType = shadingType;
		this.shapeType = shapeType;
	}

	@Override
	public Color getColor() {
		return primaryColor;
	}
	
	@Override
	public Color getSecondaryColor() {
		return secondaryColor;
	}

	@Override
	public int getStartX() {
		return startX;
	}

	@Override
	public int getStartY() {
		return startY;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}
	
	@Override
	public ShapeShadingType getShadingType() {
		return shadingType;
	}
	
	@Override
	public ShapeType getShapeType() {
		return shapeType;
	}
	
	@Override
	public Boolean isSelected() {
		return selected;
	}
	
	@Override
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@Override
	public void run() {
		PaintCanvasBase paintCanvasBase = PaintCanvas.getInstance();
		Graphics2D graphics = paintCanvasBase.getGraphics2D();
		
		switch(getShadingType()) {
			case FILLED_IN:
				graphics.setColor(getColor());
				graphics.fillRect(getStartX(), getStartY(), getWidth(), getHeight());
				break;
			case OUTLINE:
				graphics.setColor(getColor());
				graphics.setStroke(new BasicStroke(3));
				graphics.drawRect(getStartX(), getStartY(), getWidth(), getHeight());
				break;
			case OUTLINE_AND_FILLED_IN:
				graphics.setColor(getColor());
				graphics.fillRect(getStartX(), getStartY(), getWidth(), getHeight());
				graphics.setColor(getSecondaryColor());
				graphics.setStroke(new BasicStroke(3));
				graphics.drawRect(getStartX(), getStartY(), getWidth(), getHeight());
				break;
		}
		
		if(isSelected()) {
			graphics.setColor(Color.GRAY);
			Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
			graphics.setStroke(dashed);
			graphics.drawRect(getStartX()-5, getStartY()-5, getWidth() + 10, getHeight() + 10);
		}
		
	}
}
