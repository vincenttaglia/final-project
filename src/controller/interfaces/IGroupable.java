package controller.interfaces;

import view.Point;

public interface IGroupable extends IUndoable, ICommand {
	int getStartX();
	int getStartY();
	int getWidth();
	int getHeight();
	Boolean isSelected();
	void setSelected(Boolean selected);
	IGroupable transform(Point startPoint, Point endPoint);
	IGroupable transform(int transformX, int transformY);
	
	public default Boolean includesPoint(Point point) {
		return point.getX() >= getStartX() &&
				point.getX() <= getStartX() + getWidth() &&
				point.getY() >= getStartY() &&
				point.getY() <= getStartY() + getHeight();
	}
	
	public default Boolean isIncludedIn(Point startPoint, Point endPoint) {
		int startX = startPoint.getX() <= endPoint.getX() ? startPoint.getX() : endPoint.getX();
		int startY = startPoint.getY() <= endPoint.getY() ? startPoint.getY() : endPoint.getY();
		int width = Math.abs(startPoint.getX()-endPoint.getX());
		int height = Math.abs(startPoint.getY()-endPoint.getY());
		
		return startX < getStartX() + getWidth() && 
				startX + width > getStartX() &&
				startY < getStartY() + getHeight() &&
				startY + height > getStartY();
	}
}
