package controller.interfaces;

import java.awt.Color;

import controller.RedrawCommand;
import controller.ShapeBuilder;
import model.ShapeShadingType;
import model.ShapeType;
import model.persistence.GroupableList;
import view.Point;

public interface IShape extends IGroupable {
	Color getColor();
	Color getSecondaryColor();
	ShapeShadingType getShadingType();
	ShapeType getShapeType();
	
	@Override
	public default void undo() {
		GroupableList.getInstance().remove(this);
		RedrawCommand.run();
	}

	@Override
	public default void redo() {
		GroupableList.getInstance().push(this);
		run();
		
	}
	
	public default IGroupable transform(Point startPoint, Point endPoint) {
		int x = endPoint.getX() - startPoint.getX();
		int y = endPoint.getY() - startPoint.getY();
		return transform(x, y);
	}
	
	public default IGroupable transform(int transformX, int transformY) {
		return new ShapeBuilder()
			.copyShape(this)
			.setStartX(getStartX() + transformX)
			.setStartY(getStartY() + transformY)
			.build();
	}
	

}
