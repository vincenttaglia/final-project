package controller;

import java.util.ArrayList;

import controller.interfaces.IGroupable;
import model.persistence.GroupableList;
import model.persistence.SelectList;

public class GroupCommands {
	public static void group() {
		ArrayList<IGroupable> items = new ArrayList<IGroupable>(SelectList.getInstance().getSelectList());
		if(items.size() > 1) {
			IGroupable newGroup = GroupableFactory.createGroup(items);
			GroupableList.getInstance().remove(items);
			SelectList.getInstance().clear();
			GroupableList.getInstance().push(newGroup);
			SelectList.getInstance().select(newGroup);
			RedrawCommand.run();
			CommandHistory.getInstance().add(newGroup);
		}
		
		
	}
	
	public static void ungroup() {
		ArrayList<IGroupable> items = new ArrayList<IGroupable>(SelectList.getInstance().getSelectList());
		if(items.size() == 1 && items.get(0).getClass() == Group.class) {
			UngroupCommand newCmd = new UngroupCommand((Group) items.get(0));
			CommandHistory.getInstance().add(newCmd);
			newCmd.run();
		}
	}
}
