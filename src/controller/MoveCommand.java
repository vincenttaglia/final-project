package controller;

import java.util.ArrayList;

import controller.interfaces.ICommand;
import controller.interfaces.IGroupable;
import controller.interfaces.IUndoable;
import model.persistence.GroupableList;
import model.persistence.SelectList;
import view.Point;

public class MoveCommand implements ICommand, IUndoable{
	private ArrayList<IGroupable> oldShapes;
	private ArrayList<IGroupable> newShapes;
	
	
	
	public MoveCommand(ArrayList<IGroupable> oldShapes, Point startPoint, Point endPoint) {
		this.oldShapes = oldShapes;
		this.newShapes = new ArrayList<IGroupable>();
		
		for(IGroupable s : oldShapes) {
			newShapes.add(s.transform(startPoint, endPoint));
		}
	}

	@Override
	public void undo() {
		newShapes.forEach((s) -> {
			GroupableList.getInstance().remove(s);
			SelectList.getInstance().deselect(s);
		});
		oldShapes.forEach((s) -> {
			GroupableList.getInstance().push(s);
			SelectList.getInstance().select(s);
		});
		RedrawCommand.run();
	}

	@Override
	public void redo() {
		run();
		
	}

	@Override
	public void run() {
		oldShapes.forEach((s) -> {
			GroupableList.getInstance().remove(s);
			SelectList.getInstance().deselect(s);
		});
		newShapes.forEach((s) -> {
			GroupableList.getInstance().push(s);
			SelectList.getInstance().select(s);
		});
		RedrawCommand.run();
		
	}

}
