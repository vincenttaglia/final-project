package controller;

import java.awt.Color;
import java.awt.Graphics2D;

import model.persistence.GroupableList;
import view.gui.PaintCanvas;
import view.interfaces.PaintCanvasBase;

public class RedrawCommand {

	public static void run() {
		PaintCanvasBase paintCanvasBase = PaintCanvas.getInstance();
		Graphics2D graphics = paintCanvasBase.getGraphics2D();
		
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, 10000, 10000);
		
		GroupableList.getInstance().getGroupableList().forEach((s) -> {
			s.run();
		});
		
	}

}
