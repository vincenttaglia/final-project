package controller;

import model.persistence.Clipboard;

public class CopyCommand {
	public static void run() {
		Clipboard.getInstance().copy();
	}
}
