package controller;

import java.awt.Color;

import controller.interfaces.IShape;
import model.ShapeColorMap;
import model.ShapeShadingType;
import model.ShapeType;
import model.persistence.ApplicationState;
import view.Point;

public class ShapeBuilder {
	private Color primaryColor;
	private Color secondaryColor;
	private int startX;
	private int startY;
	private int width;
	private int height;
	private ShapeShadingType shadingType;
	private ShapeType shapeType;
	
	public ShapeBuilder setCurrentState(ApplicationState applicationState) {
		Color color = ShapeColorMap.getColor(applicationState.getActivePrimaryColor());
		Color secondaryColor = ShapeColorMap.getColor(applicationState.getActiveSecondaryColor());
		ShapeType shapeType = applicationState.getActiveShapeType();
		ShapeShadingType shadingType = applicationState.getActiveShapeShadingType();
		return setPrimaryColor(color).setSecondaryColor(secondaryColor).setShapeType(shapeType).setShadingType(shadingType);
	}
	
	public ShapeBuilder setByPoints(Point startPoint, Point endPoint) {
		int startX = startPoint.getX() <= endPoint.getX() ? startPoint.getX() : endPoint.getX();
		int startY = startPoint.getY() <= endPoint.getY() ? startPoint.getY() : endPoint.getY();
		int width = Math.abs(startPoint.getX()-endPoint.getX());
		int height = Math.abs(startPoint.getY()-endPoint.getY());
		return setStartX(startX).setStartY(startY).setWidth(width).setHeight(height);
	}
	
	public ShapeBuilder copyShape(IShape shape) {
		return setPrimaryColor(shape.getColor())
				.setSecondaryColor(shape.getSecondaryColor())
				.setStartX(shape.getStartX())
				.setStartY(shape.getStartY())
				.setHeight(shape.getHeight())
				.setWidth(shape.getWidth())
				.setShadingType(shape.getShadingType())
				.setShapeType(shape.getShapeType());
	}
	
	public ShapeBuilder setPrimaryColor(Color primaryColor) {
		this.primaryColor = primaryColor;
		return this;
	}
	
	public ShapeBuilder setSecondaryColor(Color secondaryColor) {
		this.secondaryColor = secondaryColor;
		return this;
	}
	
	public ShapeBuilder setStartX(int startX) {
		this.startX = startX;
		return this;
	}
	
	public ShapeBuilder setStartY(int startY) {
		this.startY = startY;
		return this;
	}
	
	public ShapeBuilder setWidth(int width) {
		this.width = width;
		return this;
	}
	
	public ShapeBuilder setHeight(int height) {
		this.height = height;
		return this;
	}
	
	public ShapeBuilder setShadingType(ShapeShadingType shadingType) {
		this.shadingType = shadingType;
		return this;
	}
	
	public ShapeBuilder setShapeType(ShapeType shapeType) {
		this.shapeType = shapeType;
		return this;
	}
	
	public IShape build() {
		return GroupableFactory.createShape(primaryColor, secondaryColor, startX, startY, width, height, shadingType, shapeType);
	}
}
