package controller;

import java.util.ArrayList;

import controller.interfaces.ICommand;
import controller.interfaces.IGroupable;
import controller.interfaces.IUndoable;
import model.persistence.GroupableList;
import model.persistence.SelectList;

public class DeleteCommand implements ICommand, IUndoable{
	ArrayList<IGroupable> deleteList;
	
	public DeleteCommand() {
		this.deleteList = new ArrayList<IGroupable>(SelectList.getInstance().getSelectList());
		CommandHistory.getInstance().add(this);
	}

	@Override
	public void undo() {
		GroupableList.getInstance().push(this.deleteList);
		RedrawCommand.run();
	}

	@Override
	public void redo() {
		run();
	}

	@Override
	public void run() {
		GroupableList.getInstance().remove(this.deleteList);
		RedrawCommand.run();
	}

}
