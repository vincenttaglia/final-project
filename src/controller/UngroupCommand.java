package controller;

import controller.interfaces.ICommand;
import controller.interfaces.IGroupable;
import controller.interfaces.IUndoable;
import model.persistence.GroupableList;
import model.persistence.SelectList;

public class UngroupCommand implements ICommand, IUndoable {
	
	Group group;
	
	public UngroupCommand(Group group) {
		this.group = group;
	}

	@Override
	public void run() {
		GroupableList.getInstance().remove(group);
		SelectList.getInstance().clear();
		GroupableList.getInstance().push(group.getGroupArray());
		for(IGroupable item : group.getGroupArray())
			SelectList.getInstance().select(item);
		RedrawCommand.run();
	}

	@Override
	public void undo() {
		GroupableList.getInstance().remove(group.getGroupArray());
		SelectList.getInstance().clear();
		GroupableList.getInstance().push(group);
		SelectList.getInstance().select(group);
		RedrawCommand.run();
		
	}

	@Override
	public void redo() {
		run();
		
	}

}
